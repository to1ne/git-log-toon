package main

import (
	"errors"
	"fmt"
	"time"

	"git-me.tech/pkg/gitme"
)

var ErrBranchNotFound = errors.New("branch not found")

var now = EventDate(time.Now().Format("2006-01-02"))

var lifeEvents = []LifeEvent{
	{
		Slug: "birth",
		Date: "1985-10-27",
		Message: `Initial commit

Toon is born on a Sunday, right at noon, "Between the soup and the potatoes" as
they would say.`,
	},
	{
		Slug: "elementary-start",
		Date: "1991-09-02",
		Message: `edu: First day of elementary school

Toon started going to the only elementary school in his home town Achel.`,
	},
	{
		Slug:    "elementary-end",
		Parents: Slugs{"elementary-start"},
		Date:    "1998-06-30",
		Message: `edu: Last day of elementary school`,
	},
	{
		Slug:    "tio-start",
		Parents: Slugs{"elementary-end"},
		Date:    "1999-09-01",
		Message: `edu: Start High school

There was an interest in technology from the start. So Toon chose to follow a
Science, Technology, Engineering, and Mathematics (STEM) education called
"Industrial Sciences" at TIO in Overpelt. This included plenty of mathematics,
combined with chemistry, physics, strength theory, and electronics.`,
	},
	{
		Slug:    "tio-end",
		Parents: Slugs{"tio-start"},
		Date:    "2003-06-30",
		Message: `edu: Graduation in Industrial Sciences`,
	},
	{
		Slug:    "khk-start",
		Parents: Slugs{"tio-end"},
		Date:    "2003-09-14",
		Message: `edu: Start Education at KHK Geel`,
	},
	{
		Slug: "web-start",
		Date: "2004-01-01",
		Message: `skill: Web development with HTML & CSS

Around this time, or maybe earlier, Toon started to build websites. Nothing
exciting yet, mostly small projects for fun and friends. The interest in web
technology never went away since.`,
	},
	{
		Slug: "c-start",
		Date: "2005-01-03",
		Message: `skill: Programming in C++

C++ was the first real programming language Toon was thaught in school. A pretty
advanced language to get started with, but it thaught him a lot about
programming in general.`,
	},
	{
		Branch:  "education",
		Slug:    "khk-end",
		Parents: Slugs{"khk-start"},
		Date:    "2008-06-30",
		Message: `edu: Graduation as Master of Science, Electronics - ICT

In 2008 Toon received his Master's degree at the KHK in Geel.

Toon's Master thesis was about speech technology.`,
	},
	{
		Slug:    "education-end",
		Parents: Slugs{"birth", "khk-end"},
		Date:    "2008-06-30",
		Message: `edu: Finished education

This marks the end of Toon's education.`,
	},
	{
		Slug: "tokheim-start",
		Date: "2008-08-01",
		Message: `job: Start at Tokheim Turnhout as Software Engineer

Software development for embedded Linux outdoor payment devices.
Achievements:
- Linux driver programming
- Hardware integration
- Event-driven & object-oriented application design in C++
- Design & implementation of software authentication & encryption
- Maintenance & Certification of EMV Level 2 Contact Terminal Application Kernel
`,
	},
	{
		Slug:    "c-tokheim",
		Parents: Slugs{"c-start"},
		Date:    "2008-08-01",
		Message: `skill: Use C/C++ professionally

At his first job Toon worked in C++. His job was building software for an
embedded Linux payment terminal device. These applications were written in C++,
but his job also involved bits of plain C when working on GNU/Linux kernel
drivers.`,
	},
	{
		Slug: "ruby-start",
		Date: "2010-01-01",
		Message: `skill: Programming in Ruby

While working with C++ all day, Toon grew an interest in Ruby and started
playing around with it in his free time.`,
	},
	{
		Slug:    "c++-course",
		Parents: Slugs{"c-tokheim"},
		Date:    "2010-10-18",
		Message: `skill: Advanced C++ course

After a few years Toon got the opportunity to follow a three-day Advanced C++
course. This was really insightful to learn about the internals of iterators and
traits and what not.

C++11 was around the corner, and there was plenty exciting stuff coming to the
language.`,
	},
	{
		Slug: "objc-start",
		Date: "2012-01-01",
		Message: `skill: Mobile development with Objective-C

After teaching himself Ruby, Toon wanted to get into mobile app development for
iOS. So he picked up a book from The Big Nerd Ranch and learned app development
with Objective-C.

He once pitched a mobile app at his employer, with a working prototype, but
there wasn't any interest because they opted for a web app instead.`,
	},
	{
		Branch:  "job/tokheim",
		Slug:    "tokheim-end",
		Parents: Slugs{"tokheim-start"},
		Date:    "2015-04-30",
		Message: `job: Resigned at Tokheim`,
	},
	{
		Slug: "10to1-start",
		Date: "2015-05-04",
		Message: `job: Start at 10to1 as Developer

At 10to1 projects for customers were built to demand. It was a great mix of
different technologies with plenty of chances to learn new things.
`,
	},
	{
		Slug:    "ruby-10to1",
		Parents: Slugs{"ruby-start"},
		Date:    "2015-05-04",
		Message: `skill: Use Ruby professionally

After playing around with Ruby in his free time, Toon finally got an opportunity
to work with those technologies professionally.`,
	},
	{
		Slug:    "web-10to1",
		Parents: Slugs{"web-start"},
		Date:    "2015-05-04",
		Message: `skill: Build websites professionally

The job at 10to1 also involved frontend work, mostly HTML and CSS. Although we
mostly used static HTML with Ruby on Rails there was also one project where
Ember.js.`,
	},
	{
		Branch:  "skill/obj-c",
		Slug:    "objc-10to1",
		Parents: Slugs{"objc-start"},
		Date:    "2015-05-04",
		Message: `skill: Use Obj-C professionally

Some projects at 10to1 involved building applications for iOS devices.`,
	},
	{
		Slug: "psql-start",
		Date: "2015-07-03",
		Message: `skill: First experience with PostgreSQL

At 10to1 the first experience was gained with the awesome RDBMS PostgreSQL.
Thanks to the friendly and skilled colleagues, Toon learned about things like
proper Index practices, Enumerated Types, Materialized Views, and more.`,
	},
	{
		Branch:  "job/10to1",
		Slug:    "10to1-end",
		Parents: Slugs{"10to1-start"},
		Date:    "2016-12-30",
		Message: `job: Resigned at 10to1`,
	},
	{
		Slug:    "objc-merge",
		Parents: Slugs{"objc-10to1"},
		Date:    "2016-12-30",
		Message: `skills: End of using Obj-C

After leaving 10to1 Toon never touched Obj-C again. No problem, because Swift
was on the rise anyway.`,
	},
	{
		Slug:    "10to1-merge",
		Parents: Slugs{"tokheim-end", "10to1-end"},
		Date:    "2016-12-30",
		Message: "job: Switching jobs",
	},
	{
		Branch:  "skill/web",
		Slug:    "web-gitlab",
		Parents: Slugs{"web-10to1"},
		Date:    "2016-11-22",
		Message: `skill: GitLab web development

During the interview process Toon was tasked to build a button (and backend
code) to delete all branches that are merged into the default branch[1]. This
change made him get the job and also was awarded as MVP for GitLab 8.14.

More of that work would follow once he was hired.

[1]: https://about.gitlab.com/releases/2016/11/22/gitlab-8-14-released/#delete-all-merged-branches`,
	},
	{
		Slug: "gitlab-start",
		Date: "2017-01-02",
		Message: `job: Start at GitLab as Backend Developer

Toon started in the so-called Platform team at GitLab. That team was responsible
for Git operations, Merge Requests, and more.`,
	},
	{
		Branch:  "skill/ruby",
		Slug:    "ruby-gitlab",
		Parents: Slugs{"ruby-10to1"},
		Date:    "2017-01-02",
		Message: `skill: Backend with in Ruby

Most of the GitLab codebase is written in Ruby. While working in the GitLab
Platform team, Toon dealed a lot with background workers powered by Sidekiq[1].

[1]: https://sidekiq.org`,
	},
	{
		Slug:    "gitlab-api",
		Parents: Slugs{"gitlab-start"},
		Date:    "2017-03-22",
		Message: `achievement: Lead introduction of API v4

Toon was responsible of the maintenance of the REST API, and he led the path to
introduce API v4. All deprecations and breaking changes we wanted to make had to
happen in the upcoming major release GitLab v9.0.`,
	},
	{
		Slug:    "gitlab-geo",
		Parents: Slugs{"gitlab-api"},
		Date:    "2017-06-01",
		Message: `job: Transition to GitLab Geo team

After working in the Platform team, Toon moved to the Geo team. That team is
responsible of building a Geo-distributed solution into GitLab. With GitLab Geo
customers can install GitLab at their different offices and have them
synchronize data between those installations. This allows their employees to
have quick access to everything to do their job.
`,
	},
	{
		Slug: "golang-start",
		Date: "2017-11-15",
		Message: `skill: Programming in Golang

Because GitLab uses Golang for a few components, Toon started to learn about Go.
He wrote some tools to learn about the language.`,
	},
	{
		Slug:    "gcp-migration",
		Date:    "2018-07-28",
		Parents: Slugs{"gitlab-geo"},
		Message: `achievement: Contribute to the migration to GCP

In 2018 GitLab decided to migrate from Azure to GCP. GitLab Geo played a key
role in this migration. It allowed us to have a hot standby in GCP, so the
migration could happen with a minimal on downtime.

Many skilled people, including Toon as one of the experts on the Geo team, made
the migration happen as smooth as possible.

[1]: https://about.gitlab.com/blog/2018/06/25/moving-to-gcp/`,
	},
	{
		Branch:  "skill/postgres",
		Slug:    "psql",
		Parents: Slugs{"psql-start"},
		Date:    "2019-06-28",
		Message: `skill: PostgreSQL database performance reviews

There was a need for people to review changes related to database use. It was
then Toon stepped forward and dove deep into PostgreSQL and became [1] a
database maintainer responsible of reviewing merge requests that make changes to
how the database is used, and ensure those changes work at scale.

[1]: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/4710`,
	},
	{
		Slug:    "gitlab-promotion",
		Parents: Slugs{"gcp-migration"},
		Date:    "2019-11-25",
		Message: `job: Promoted to Senior Backend Engineer

After years of hard work Toon's efforts were recognized with a promotion to
Senior Backend Engineer.`,
	},
	{
		Branch:  "job/gitlab",
		Slug:    "gitlab-gitaly",
		Parents: Slugs{"gitlab-promotion"},
		Date:    "2020-10-01",
		Message: `Transition to Gitaly team`,
	},
	{
		Branch:  "skill/c",
		Slug:    "c-git",
		Date:    "2020-05-26",
		Parents: Slugs{"c++-course"},
		Message: `skill: Learning about the Git source code

Interest in the Git[1] project kept growing, Toon wanted to be involved in that
project. Toon participated on the mailing list and when he was getting into
SHA256 support[2] he managed to get his first changes merged.

[1]: https://git-scm.com
[2]: https://git-scm.com/docs/hash-function-transition
`,
	},
	{
		Branch:  "skill/golang",
		Slug:    "golang-gitaly",
		Parents: Slugs{"golang-start"},
		Date:    "2020-10-02",
		Message: `skill: Use golang professionally

When Toon moved to the Gitaly team his day-to-day programming language changed
from Ruby to Golang.`,
	},

	{
		Branch:  "skills",
		Slug:    "skills",
		Parents: Slugs{"objc-merge", "web-gitlab", "c-git", "golang-gitaly", "ruby-gitlab", "psql"},
		Date:    now,
		Message: `skill: All my technical skills`,
	},
	{
		Branch:  "jobs",
		Slug:    "jobs",
		Parents: Slugs{"10to1-merge", "gitlab-gitaly"},
		Date:    now,
		Message: "job: Professional career until now",
	},
	{
		Branch:  "toon",
		Slug:    "me-now",
		Parents: Slugs{"education-end", "jobs", "skills"},
		Date:    now,
		Message: `This is me now

You can find me on Twitter at @to1ne[1]. I sometimes blog at
https://writepermission.com and there's more to read about me at
https://iotcl.com.

[1]: https://twitter.com/to1ne
`,
	},
}

// WriteCommits writes commits to git repo at repoPath
func WriteCommits(repoPath string) error {
	gitter := gitme.NewGitter(repoPath)

	if err := gitter.Init(); err != nil {
		return fmt.Errorf("Write commits: %v", err)
	}

	gitter.SetAuthor("Toon Claes", "toon@iotcl.com")

	for i := range lifeEvents {
		event := &lifeEvents[i] // address events by reference
		if err := event.Commit(gitter); err != nil {
			return fmt.Errorf("Write commits at %s: %v", event.Slug, err)
		}
	}

	if err := gitter.SetDefaultBranch("toon"); err != nil {
		return fmt.Errorf("Set default branch: %w", err)
	}

	return nil
}

// BranchNames returns all the branches.
func BranchNames() []string {
	var branches []string

	for _, e := range lifeEvents {
		if e.Branch != "" {
			branches = append(branches, e.Branch)
		}
	}

	return branches
}

// BranchTip return the tip LifeEvent for the given branch name, or an error.
func BranchTip(branch string) (*LifeEvent, error) {
	for _, e := range lifeEvents {
		if e.Branch == branch {
			return &e, nil
		}
	}
	return nil, ErrBranchNotFound
}

func Find(slug string) (*LifeEvent, error) {
	for _, e := range lifeEvents {
		if e.Slug == slug {
			return &e, nil
		}
	}
	return nil, ErrBranchNotFound
}
