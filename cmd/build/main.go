package main

import (
	"errors"
	"log"
	"os"

	"git-me.tech/pkg/gitme"
)

const (
	repoPath = "toon.git"
)

func main() {
	if err := os.RemoveAll(repoPath); err != nil && !errors.Is(err, os.ErrNotExist) {
		log.Fatalf("Failed to clean old repo: %v", err)
	}

	if err := WriteCommits(repoPath); err != nil {
		log.Fatal(err)
	}

	for _, b := range BranchNames() {
		if err := gitme.Htmlize(repoPath, b); err != nil {
			log.Fatal(err)
		}
		if err := Plainize(repoPath, b); err != nil {
			log.Fatal(err)
		}
	}

	log.Println("Finished building")
}
