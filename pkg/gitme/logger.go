package gitme

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"
)

const commitLogFormat = `--format=` +
	`%H` + // commit hash
	`%x1F` + // unit separator
	`%P` + // parent hashes
	`%x1F` + // unit separator
	`%as` + // author date
	`%x1F` + // unit separator
	`%D` + // ref names
	`%x1F` + // unit separator
	`%s` + // subject
	`%x1F` + // unit separator
	`%b` + // body
	`%x1C` // file separator

// Logger can be used to get a log of Commits.
type Logger struct {
	s *bufio.Scanner
}

func NewLogger(repoPath string, extraArgs ...string) (Logger, func(), error) {
	gitter := NewGitter(repoPath)

	cmd := gitter.cmd("log", append([]string{commitLogFormat}, extraArgs...)...)

	r, err := cmd.StdoutPipe()
	if err != nil {
		return Logger{}, nil, fmt.Errorf("NewLogger: open stdout pipe: %w", err)
	}

	s := bufio.NewScanner(r)
	s.Split(logSplit)

	if err := cmd.Start(); err != nil {
		return Logger{}, nil, fmt.Errorf("NewLogger: git command start: %w", err)
	}

	return Logger{s: s},
		func() {
			cmd.Wait()
			r.Close()
		},
		nil
}

func (l Logger) Scan() bool {
	return l.s.Scan()
}

func (l Logger) Commit() (Commit, error) {
	fields := strings.Split(l.s.Text(), "\x1F")

	if len(fields) != 6 {
		return Commit{}, fmt.Errorf("incorrect number of commit fields %d", len(fields))
	}

	var parents []Oid
	for _, p := range strings.Split(fields[1], " ") {
		if p == "" {
			continue
		}
		parents = append(parents, Oid(p))
	}

	var refNames []string
	if ref := strings.TrimSpace(fields[3]); len(ref) > 0 {
		refNames = append(refNames, strings.TrimPrefix(ref, "HEAD -> "))
	}

	return Commit{
			Oid:      Oid(strings.TrimSpace(fields[0])),
			Parents:  parents,
			Date:     fields[2],
			RefNames: refNames,
			Title:    fields[4],
			Body:     fields[5],
		},
		nil
}

func logSplit(data []byte, atEOF bool) (int, []byte, error) {
	if atEOF {
		if data[len(data)-1] != 0x1C {
			return 0, nil, fmt.Errorf("last record incomplete")
		}
		return len(data), data, nil
	}

	ind := bytes.Index(data, []byte{0x1C})
	if ind < 0 {
		// Ask more data
		return 0, nil, nil
	}
	return ind + 1, data[:ind], nil
}
