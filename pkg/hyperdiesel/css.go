package hyperdiesel

import (
	"fmt"
	"strings"
)

// StyleSheet is the type to build a CSS stylesheet with code.
type StyleSheet struct {
	rules []*StyleRule
}

type declaration struct {
	property string
	value    string
}

// StyleRule is a single rule with one or many selectors and one or many
// declarations.
type StyleRule struct {
	selectors    []string
	declarations []declaration
}

// NewStyleSheet creates a new StyleSheet.
func NewStyleSheet() *StyleSheet {
	return &StyleSheet{}
}

// NewStyleRule creates a new style rule.
func NewStyleRule(selectors ...string) *StyleRule {
	return &StyleRule{
		selectors:    selectors,
		declarations: []declaration{},
	}
}

// Rule creates a new rule in the stylesheet.
func (sheet *StyleSheet) Rule(selectors ...string) *StyleRule {
	for _, rule := range sheet.rules {
		if len(rule.selectors) != len(selectors) {
			continue
		}
		identical := true
		for i, sel := range rule.selectors {
			if sel != selectors[i] {
				identical = false
				continue
			}
		}
		if identical {
			return rule
		}
	}

	r := NewStyleRule(selectors...)
	sheet.rules = append(sheet.rules, r)

	return r
}

// String formats the CSS stylesheet to a string.
func (sheet *StyleSheet) String() string {
	var out strings.Builder

	for _, r := range sheet.rules {
		out.WriteString(r.String())
		out.WriteString("\n")
	}

	return out.String()
}

// Declare a property with a value.
func (r *StyleRule) Declare(property, value string) *StyleRule {
	r.declarations = append(r.declarations,
		declaration{
			property: property,
			value:    value,
		})

	return r
}

// String formats a StyleRule to a string.
func (r *StyleRule) String() string {
	var out strings.Builder

	out.WriteString(strings.Join(r.selectors, ",\n"))
	out.WriteString(" {\n")

	for _, d := range r.declarations {
		out.WriteString(fmt.Sprintf("  %v: %v;\n", d.property, d.value))
	}

	out.WriteString("}\n")

	return out.String()
}

func (r *StyleRule) StyleString() string {
	var out strings.Builder
	var other bool

	for _, d := range r.declarations {
		if other {
			out.WriteString("; ")
		}
		other = true

		out.WriteString(fmt.Sprintf("%v: %v", d.property, d.value))
	}

	return out.String()
}
