package css

import (
	"fmt"
	"strings"
)

// Sum returns the sum of all arguments as a calc() CSS function call.
func Sum(args ...string) string {
	return fmt.Sprintf("calc(%v)", strings.Join(args, " + "))
}

// Max returns the max of all arguments as a max() CSS function call.
func Max(args ...string) string {
	return fmt.Sprintf("max(%v)", strings.Join(args, ", "))
}
