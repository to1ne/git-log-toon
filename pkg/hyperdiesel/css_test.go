package hyperdiesel

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewStyleSheet(t *testing.T) {
	css := NewStyleSheet()

	div := css.Rule("div")
	div.Declare("display", "flex")
	div.Declare("background", "hotpink")

	headings := css.Rule("h1", "h2", "h3")
	headings.Declare("font-weight", "bold")
	headings.Declare("color", "red")

	expected := `div {
  display: flex;
  background: hotpink;
}

h1,
h2,
h3 {
  font-weight: bold;
  color: red;
}

`

	require.Equal(t, expected, css.String())
}

func TestStylesheet_Rule(t *testing.T) {
	css := NewStyleSheet()

	div := css.Rule("div")
	div.Declare("display", "none")
	css.Rule("main").Declare("display", "block")
	div2 := css.Rule("div")

	require.Same(t, div, div2)
	require.Equal(t, div.declarations, div2.declarations)
}
