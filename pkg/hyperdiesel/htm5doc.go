package hyperdiesel

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// Doc is the type for a HTML document. It has one child Element for <html>.
type Doc struct {
	Html *Element
}

// Html5Doc returns a HTML5 document.
func Html5Doc() *Doc {
	return &Doc{
		Html: New("html"),
	}
}

// String returns the document as a string.
func (doc *Doc) String() string {
	return "<!doctype html>" + doc.Html.String()
}

// PrettyPrint prints the document nicely formatted to the given io.Writer.
func (doc *Doc) PrettyPrint(w io.Writer) (int, error) {
	var n, total int
	var err error

	buffer := bufio.NewWriter(w)

	n, err = buffer.WriteString("<!doctype html>\n")
	if err != nil {
		return n, fmt.Errorf("Pretty print doc: %w", err)
	}
	total += n

	n, err = prettyPrintEl(buffer, 0, doc.Html)
	if err != nil {
		return n, fmt.Errorf("Pretty print html: %w", err)
	}
	total += n

	err = buffer.Flush()
	if err != nil {
		return n, fmt.Errorf("Pretty print flush: %w", err)
	}

	return total, nil
}

func prettyPrintEl(w io.Writer, level int, el *Element) (int, error) {
	var n, total int
	var err error

	if el.inline() {
		n, err = printIndented(w, level, el.String())
		if err != nil {
			return n, fmt.Errorf("Pretty print element %q: %w", el.tag, err)
		}

		return total + n, nil
	}

	n, err = printIndented(w, level, el.openTag())
	if err != nil {
		return n, fmt.Errorf("Pretty print open tag %q: %w", el.tag, err)
	}
	total += n

	for _, c := range el.children {
		if chel, ok := c.(*Element); ok {
			prettyPrintEl(w, level+1, chel)
		} else {
			n, err = printIndented(w, level+1, c.String())
			if err != nil {
				return n, fmt.Errorf("Pretty print child %w", err)
			}
			total += n
		}
	}

	if !el.void {
		n, err = printIndented(w, level, el.closeTag())
		if err != nil {
			return n, fmt.Errorf("Pretty print close tag %q: %w", el.tag, err)
		}
		total += n
	}

	return total, nil
}

func printIndented(w io.Writer, level int, str string) (int, error) {
	return fmt.Fprintf(w, "%v%v\n", strings.Repeat("  ", level), str)
}
