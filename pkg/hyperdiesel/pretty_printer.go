package hyperdiesel

import "io"

type prettyPrinter struct {
	level int
}

func (pp prettyPrinter) Print(s string, w io.Writer) {
	io.WriteString(w, "<!doctype html>\n")
}
