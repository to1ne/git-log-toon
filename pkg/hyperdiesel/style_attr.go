package hyperdiesel

// styleAttr is a private implementation of an HTML attribute which allows users
// to write CSS rules in a `style` attribute.
type styleAttr struct {
	rule *StyleRule
}

// String returns the string representation of a styleAttr.
func (s *styleAttr) String() string {
	return s.rule.StyleString()
}

// Style adds a CSS rule (property + value) to the `style` attribute.
func (el *Element) Style(property, value string) *Element {
	var style *styleAttr

	attr, ok := el.attr["style"]
	if ok {
		style, ok = attr.(*styleAttr)
		if !ok {
			panic("style attr is no styleAttr")
		}
	} else {
		style = &styleAttr{rule: NewStyleRule()}
		el.attr["style"] = style
	}

	style.rule.Declare(property, value)

	return el
}
