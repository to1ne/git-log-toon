package hyperdiesel

// Text is a simple type to wrap a string primitive type into a fmt.Stringer.
type Text string

// String returns original string.
func (t Text) String() string {
	return string(t)
}
